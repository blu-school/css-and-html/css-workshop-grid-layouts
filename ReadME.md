# CSS Workshop - CSS Grid Layouts

Recording: [MS Teams Meeting](https://blubitoag-my.sharepoint.com/:v:/g/personal/s_petrov_blubito_com/EWVqpwFOxP5PtZ0Cbmjb820BBBQpZXmr05884oaN3eiMrQ)

Additional links:

- [Learn css grid](https://learncssgrid.com/)
- [A Complete Guide to Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
- [Grid cheatsheet](https://grid.malven.co/)

## Using the project

```
1) Isntall dependencies
npm install OR yarn install

2) Start project
npm run dev OR yarn dev

```
